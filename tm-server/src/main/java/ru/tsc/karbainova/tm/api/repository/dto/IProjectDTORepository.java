package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectDTORepository extends IDTORepository<ProjectDTO> {
    void clear();

    void clearByUserId(@NotNull String userId);

    ProjectDTO findByName(@Nullable String userId, @Nullable String name);

    List<ProjectDTO> findAll();

    @NotNull List<ProjectDTO> findAllByUserId(@Nullable String userId);

    @Nullable ProjectDTO findByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable ProjectDTO findByIndex(@Nullable String userId, @NotNull Integer index);

    void remove(@NotNull ProjectDTO entity);

    void removeById(@Nullable String id);

    void removeByName(@Nullable String userId, @Nullable String name);

    void removeByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, int index);

    int getCount();

    int getCountByUser(@NotNull String userId);
}
